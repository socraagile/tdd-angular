# Meetup 22.1.2020 challenge

The objective of this workshop is doing Test-Driven Development during the development of an Angular component for the edition of an address.
The edition is assisted by live-generated and selectable proposals from SwissPost directory API.

## Requirements

The specification for this address edition component is the following:

* A simple input field that is reactive to typed characters.
* At the beginning every new typed character triggers a search.
* A search is usually based on a term which is the current value of the input field.
* A search consumes the backend API that proxies the SwissPost AddressChecker web service.
* The backend API returns a list of addresses for a given set of criteria essentially based on street and zip.
* The result has to be presented right under the input field.
* Every result item is clickable and a the serialized address value of the clicked item has to be deposed into the input field.
* A result composed by a single item is not presented, but the serialized address value of item has to be deposed into the input field.

## Warm up

* Install JDK 11
* Install Node 11.x
* Clone the Angular project repository from [here](https://vondacho@bitbucket.org/socraagile/tdd-angular.git)
* Clone the SpringBoot project repository from [here](https://vondacho@bitbucket.org/noiasquad/myoffice-address.git)
    * Run the RestApplication class
    * Visit the API documentation [here](http://localhost:8080/swagger-ui.html#/address-verification-endpoint/findAddressesUsingGET)
    * Make some queries to SwissPost API using myoffice-address proxy ([sample](http://localhost:8080/api/address/v1/addresses/search?street=precossy&city=nyon))
    
## Existing stuff

* The **master** branch contains a green-field project without Karma & Jasmine but with Jest fresh configured
    * See [Install Jest on your Angular project](https://itnext.io/how-i-do-configure-jest-to-test-my-angular-8-project-2bd84a21d725)
* The **search-field-variant-1** branch proposes an incomplete solution using Reactive Form control
* The **search-field-variant-2** branch proposes a complete and working solution without Reactive Form control

## Code of Conduct

Every developer develops on his own branch.

## Your challenge

Choose one challenge inside the list below. You have 60 minutes.

### Challenge 1

To develop the Angular component from scratch.

### Challenge 2

Use TDD to add one more feature to the existing solution hosted on branch **search-field-variant-2**:

* To limit the number of REST calls to the remote API, cancelling low queries.
* To limit the number of REST calls to the remote API, ignoring early queries and triggering only the last request, 
after a predefined amount of time without any new request.

### Challenge 3

Use TDD to add one more feature to the existing solution hosted on branch **search-field-variant-2**:

* To integrate the address editing component into a Reactive Form into another component (it can be the AppComponent) 
which the edition of Person data.
    * See [Custom Reactive Form Control](https://javascript-conference.com/blog/angular-reactive-forms-building-custom-form-controls/)

### Challenge 4

To develop a Consumer-Driven Contract that specifies the interaction between frontend and backend, 
which is usable for developing the backend API in a test-driven way.
* See [Pact foundation npm](https://www.npmjs.com/package/@pact-foundation/pact)
* See [Pact foundation Documentation](https://docs.pact.io/)

## Test-Driven development

The challenge requires to development using Outside-in TDD practice, starting from a minimal component with mocked collaborators,
and progressively define the next dependent services that consume the remote API.

* One baby step after another
* Add small features to the existing stuff
    * Start with the specification of the need via a test
    * Implement the minimum of production code to make it pass
    * Refactor production code against duplication and following SOLID principles
    * Refactor test code

### Feedback loop

Run `npm run test:watch` to execute the unit tests via [Jest](https://jestjs.io/) and relaunch them on any modification

### Focus unit tests on a single specification file

Press `p` and select a filename pattern.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.


