import { TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { By } from '@angular/platform-browser';

describe('AppComponent', () => {
  it('should create the component with one input field of type text', () => {
    // given
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
    });
    const fixture = TestBed.createComponent(AppComponent);
    const componentUT = fixture.componentInstance;
    const inputElementUT = fixture.debugElement.query(By.css('input[type=text]'));
    // when
    // then
    expect(componentUT).toBeDefined();
    expect(inputElementUT).not.toBeNull();
  });
});
